'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  cors: {
    enable: true,
    package: 'egg-cors',
  },

  mysql: {
    enable: true,
    package: 'egg-mysql',
  },

  sequelize: {
    enable: true,
    package: 'egg-sequelize',
  },

  redis: {
    enable: true,
    package: 'egg-redis',
  },

  alinode: {
    enable: true,
    package: 'egg-alinode',
  },

  jwt: {
    enable: true,
    package: 'egg-jwt',
  },

  validate: {
    enable: true,
    package: 'egg-validate',
  },

  swaggerdoc: {
    enable: true,
    package: 'egg-swagger-doc',
  },

  mailer: {
    enable: true,
    package: 'egg-mailer',
  },
};
