/* eslint valid-jsdoc: "off" */

'use strict';

const path = require('path');

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1634887993852_1491';

  // 加载 errorHandler 中间件
  config.middleware = [ 'errorHandler' ];
  // 只对 /* 前缀的 url 路径生效
  config.errorHandler = {
    match: '/*',
  };

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    file: {
      // 自己静态资源存放的路径
      disk: '/public/upload',
    },
  };

  config.jwt = {
    secret: 'wmt123456', // 自定义 token 的加密条件字符串
  };

  // 跨域设置
  config.security = {
    csrf: {
      enable: false,
    },
    domainWhiteList: [ '*' ],
  };

  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
  };

  config.validate = {
    convert: true, // 对参数可以使用 convertType 规则进行类型转换
    // validateRoot: false, // 限制被验证值必须是一个对象
    widelyUndefined: true,
  };

  config.swaggerdoc = {
    dirScanner: './app/controller', // 插件扫描的文档路径
    apiInfo: {
      title: 'egg-api',
      description: 'egg-api doc',
      version: '1.0.0',
    },
    consumes: [ 'application/json', 'multipart/form-data' ], // 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html
    produces: [ 'application/json', 'multipart/form-data' ], // 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
    schemes: [ 'http', 'https' ],
    securityDefinitions: {
      apikey: {
        type: 'apiKey',
        name: 'Authorization', // jwt一般是这名字，根据需要调整
        in: 'header',
      },
      // oauth2: {
      //   type: 'oauth2',
      //   tokenUrl: 'http://127.0.0.1:7000/api/v1/users/login',
      //   flow: 'password',
      //   scopes: {
      //     'write:access_token': 'write access_token',
      //     'read:access_token': 'read access_token',
      //   },
      // },
    },
    enableSecurity: true,
    routerMap: true, // 是否自动生成route
    enable: true,
  };

  config.mailer = {
    host: 'smtp.qq.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: '2726653264@qq.com', // generated ethereal user
      pass: 'xjxugmcxybsddecb', // generated ethereal password
    },
  };

  config.alinode = {
    enable: true,
    appid: '90063',
    secret: 'a5f32da3839d3fc091b7ec48e0266e3b09bfdc86',
  };

  config.session = {
    maxAge: 24 * 3600 * 1000, // ms
    key: 'EGG_SESS',
    httpOnly: true,
    signed: false,
    encrypt: false,
    renew: true,
  };

  config.multipart = {
    // 文件上传模式，采用临时文件的模式。
    mode: 'file',
    // 配置临时文件目录，修改成项目根目录下tmp中存放，方便管理
    tmpdir: path.join(__dirname, '..', 'tmp', appInfo.name),
    // 允许上传类型的白名单
    // fileExtensions: [ '.jpg', '.jpeg', '.png', '.gif', '.bmp', '.webp' ],
    //  文件大小限制
    fileSize: '20mb',
    cleanSchedule: { cron: '0 30 4 * * *' },
  };

  config.publicKey = `
  -----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDo9td2/8xmBb82k57wKRVf+pLn
1/PfQKUWGu5jmEkGfb9Vxt2wTNBKh3f2pzKCyeIml0XU5ifA6bq8Dvk28VBuKzyB
5DgIYrF7o5MCABWX8LjFtIQjElAI8cRUYxF/6Fqu99bmjQ/KIKc8GzpF9bS1ZQJt
onZsoc3KUnPP7QBUaQIDAQAB
-----END PUBLIC KEY-----`;

  config.privateKey = `
  -----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDo9td2/8xmBb82k57wKRVf+pLn1/PfQKUWGu5jmEkGfb9Vxt2w
TNBKh3f2pzKCyeIml0XU5ifA6bq8Dvk28VBuKzyB5DgIYrF7o5MCABWX8LjFtIQj
ElAI8cRUYxF/6Fqu99bmjQ/KIKc8GzpF9bS1ZQJtonZsoc3KUnPP7QBUaQIDAQAB
AoGAP66yaaI1QrJRed7StL3WTP1XSS88SAwI1MkZ33nShIa9VfpdwYLN8YGkpnVA
FoaPXpM9kJw1w8Ngu2WwdHd0z5NXdfFdZKd+0+ngzl8VLX8q7GH8pTEDw8HTZIKE
w2d6voMHILQ9JrCoVY1XgzejWKJ2WUKWf/rLmfJ91QPqhHECQQD1yvaFcCOil9d0
EhjHMKpGYhciOmRMzWVOtf2m2xgvSdNg8CKlEoT/tNoYsXeKZ8/ZjoHx5wtr1cH+
/93etoE1AkEA8qN/RPRc4rBKtSi9cLAMVyb5m6IU4tkCaX61TkN+4g+WC3QQzuJo
CopLLfljC07zUp93ISbtngD+77Mc8VvA5QJBANjKZ51SR1wFqVcLDyfBWK4znkvm
TMkg8JX4Fv2wiShMaVA6/ZOzPfN7xFG7VcCRx9YPx7VhH8XBCBat/zeMqXECQQDn
IcHRkKmOs3pm+akD2F/ZAWKS06HruebnUXEqXXdXpps6y9RbDycmqzgUUlT757g3
w3n61ZdShQZUlZ2DePDVAkEAoews6/LB5Tzz4rDNtdHdzb5bQWWpfa2g4GqMkGq2
maehyLZ4Fd8oEu1beeSHOSd4/roNRZXdHEuYTAvJv5xgCw==
-----END RSA PRIVATE KEY-----`;

  return {
    ...config,
    ...userConfig,
  };
};
