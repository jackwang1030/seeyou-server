'use strict';

exports.security = {
  csrf: {
    enable: false,
  },
};


exports.logger = {
  dir: './logs/local', // 打印目录重定向
  outputJSON: true, // json格式输出
};

exports.mysql = {
  // 单数据库信息配置
  client: {
    // host
    host: '127.0.0.1',
    // 端口号
    port: '3306',
    // 用户名
    user: 'root',
    // 密码
    password: '123456',
    // 数据库名
    database: 'egg-api',
  },
  // 是否加载到 app 上，默认开启
  app: true,
  // 是否加载到 agent 上，默认关闭
  agent: false,
};

exports.sequelize = {
  dialect: 'mysql',
  host: '127.0.0.1',
  port: 3306,
  password: '123456',
  database: 'egg-api',
  timezone: '+08:00',
  define: {
    raw: true,
    underscored: false,
    charset: 'utf8mb4',
    timestamp: true,
    lastLogin: 'last_login',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  },
  dialectOptions: {
    dateStrings: true,
    typeCast: true,
    // collate: 'utf8mb4_general_ci',
  },
};

exports.redis = {
  client: {
    port: 6379,
    host: '127.0.0.1',
    password: '123456',
    db: 1,
  },
};
