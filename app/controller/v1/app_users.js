'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller APP用户模块
 */

class AppUserController extends Controller {

  /**
   * @summary App用户登录
   * @description App用户登录接口
   * @router post /api/v1/app_users/login
   * @request body appUserLoginReq
   * @response 200 baseResponse
   */
  async login() {
    const { ctx } = this;
    ctx.validate(ctx.rule.appUserLoginReq, ctx.request.body);
    return await ctx.service.appUsers.login(ctx.request.body);
  }

  /**
   * @summary App用户注册
   * @description App用户注册接口
   * @router post /api/v1/app_users/register
   * @request body appUserRegisterReq
   * @response 200 baseResponse
   */
  async register() {
    const { ctx } = this;
    ctx.validate(ctx.rule.appUserRegisterReq, ctx.request.body);
    return await ctx.service.appUsers.register(ctx.request.body);
  }
}

module.exports = AppUserController;
