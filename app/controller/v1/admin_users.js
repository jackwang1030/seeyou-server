'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 系统用户模块
 */

class AdminUserController extends Controller {

  /**
   * @summary 获取系统用户列表
   * @apikey
   * @description 获取系统用户列表接口
   * @router get /api/v1/admin_users
   * @request query string user_no 账号
   * @request query string phone 手机号
   * @request query string username 用户名
   * @request query string email 邮箱
   * @request query number gender 性别(1. 男, 2. 女, 3. 保密)
   * @request query number state 状态(1. 启用, 2. 禁用)
   * @request query number state 状态(1. 启用, 2. 禁用)
   * @request query string startTime 开始时间
   * @request query string endTime 结束时间
   * @request query string orderColumn 排序字段
   * @request query string orderType 排序方式
   * @request query number pageSize 页数
   * @request query number current 页码
   * @response 200 baseResponse
   */
  async index() {
    const { ctx } = this;
    const { allRule, query } = ctx.helper.findAllParamsDeal(
      ctx, {
        rule: ctx.rule.adminUserSearchReq,
        queryOrigin: ctx.query,
      });
    ctx.validate(allRule, query);
    return await ctx.service.adminUsers.index(query, ctx.query);
  }

  /**
   * @summary 创建系统用户
   * @apikey
   * @description 创建系统用户
   * @router post /api/v1/admin_users
   * @request body adminUserCreateReq
   * @response 200 baseResponse
   */
  async create() {
    const { ctx } = this;
    ctx.logger.info('系统用户新增参数', ctx.request.body);
    ctx.validate(ctx.rule.adminUserCreateReq, ctx.request.body);
    return await ctx.service.adminUsers.create(ctx.request.body);
  }

  /**
   * @summary 修改系统用户
   * @apikey
   * @description 修改系统用户接口
   * @router put /api/v1/admin_users
   * @request body adminUserPutReq
   * @response 200 baseResponse
   */
  async update() {
    const { ctx } = this;
    ctx.validate(ctx.rule.adminUserPutReq, ctx.request.body);
    return await ctx.service.adminUsers.update(ctx.request.body);
  }

  /**
   * @summary 系统用户修改密码
   * @apikey
   * @description 系统用户修改密码接口
   * @router put /api/v1/admin_users/updatePwd
   * @request body adminUserUpPwdReq
   * @response 200 baseResponse
   */
  async updatePwd() {
    const { ctx } = this;
    ctx.validate(ctx.rule.adminUserUpPwdReq, ctx.request.body);
    return await ctx.service.adminUsers.updatePwd(ctx.request.body);
  }

  /**
   * @summary 系统用户登录
   * @description 系统用户登录接口
   * @router post /api/v1/admin_users/login
   * @request body adminUserLoginReq
   * @response 200 baseResponse
   */
  async login() {
    const { ctx } = this;
    ctx.validate(ctx.rule.adminUserLoginReq, ctx.request.body);
    return await ctx.service.adminUsers.login(ctx.request.body);
  }

  /**
   * @summary 系统用户获取个人信息
   * @apikey
   * @description 系统用户获取个人信息接口
   * @router get /api/v1/admin_users/getInfo
   * @response 200 baseResponse
   */
  async getInfo() {
    const { ctx } = this;
    return await ctx.service.adminUsers.getInfo();
  }

  /**
   * @summary 更改系统用户状态
   * @apikey
   * @description 更改系统用户状态接口
   * @router put /api/v1/admin_users/updateState
   * @request body adminUserChangeState
   * @response 200 baseResponse
   */
  async updateState() {
    const { ctx } = this;
    ctx.validate(ctx.rule.adminUserChangeState, ctx.request.body);
    return await ctx.service.adminUsers.updateState(ctx.request.body);
  }

  /**
   * @summary 退出登录
   * @apikey
   * @description 退出登录接口
   * @router post /api/v1/admin_users/logout
   * @response 200 baseResponse
   */
  async logout() {
    const { ctx } = this;
    return await ctx.service.adminUsers.logout();
  }
}

module.exports = AdminUserController;
