'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 系统配置模块
 */

class SettingsController extends Controller {

  /**
   * @apikey
   * @summary 获取系统配置
   * @description 获取系统配置接口
   * @router get /api/v1/settings
   */
  async index() {
    const { ctx } = this;
    return ctx.service.settings.index();
  }

  /**
   * @summary 系统配置修改
   * @apikey
   * @description 系统配置修改接口
   * @router post /api/v1/settings
   * @request body settings
   * @response 200 baseResponse
   */
  async update() {
    const { ctx } = this;
    ctx.validate(ctx.rule.settings, ctx.request.body);
    return ctx.service.settings.update(ctx.request.body);
  }
}

module.exports = SettingsController;
