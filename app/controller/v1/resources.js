'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 资源(菜单，权限)模块
 */
class ResourcesController extends Controller {

  /**
   * @summary 新增资源(菜单，权限)
   * @apikey
   * @description 新增资源(菜单，权限)接口
   * @router post /api/v1/resources
   * @request body resourcesReq
   * @response 200 baseResponse
   */
  async add() {
    const { ctx } = this;
    ctx.validate(ctx.rule.resourcesReq, ctx.request.body);
    return await ctx.service.resources.add(ctx.request.body);
  }

  /**
   * @summary 修改资源(菜单，权限)
   * @apikey
   * @description 修改资源(菜单，权限)接口
   * @router put /api/v1/resources
   * @request body resourcesPutReq
   * @response 200 baseResponse
   */
  async update() {
    const { ctx } = this;
    ctx.validate(ctx.rule.resourcesPutReq, ctx.request.body);
    return await ctx.service.resources.update(ctx.request.body);
  }

  /**
   * @summary 删除资源(菜单，权限)
   * @apikey
   * @description 删除资源(菜单，权限)接口
   * @router delete /api/v1/resources
   * @request body resourcesDelReq
   * @response 200 baseResponse
   */
  async destroy() {
    const { ctx } = this;
    ctx.validate(ctx.rule.resourcesDelReq, ctx.request.body);
    return await ctx.service.resources.destroy(ctx.request.body);
  }

  /**
   * @summary 获取所有资源(菜单，权限)
   * @apikey
   * @description 获取所有资源(菜单，权限)接口
   * @router get /api/v1/getAllResources
   * @request query number status 状态
   * @request query string name_cn 中文名称
   * @request query number type* 类型 1.list形式 2.tree形式
   * @response 200 baseResponse
   */
  async getAllResources() {
    const { ctx } = this;
    ctx.validate(ctx.rule.resourcesSeaReq, ctx.request.query);
    return await ctx.service.resources.getAllResources(ctx.request.query);
  }

  /**
   * @summary 获取资源tree(菜单，权限)
   * @apikey
   * @description 获取资源tree(菜单，权限)接口
   * @router get /api/v1/getTreeResources
   * @request query string kindIds 资源类型
   * @response 200 baseResponse
   */
  async getTreeResources() {
    const { ctx } = this;
    return await ctx.service.resources.getTreeResources(ctx.request.query);
  }

  /**
   * @summary 用户获取菜单
   * @apikey
   * @description 用户获取菜单接口
   * @router get /api/v1/getRouters
   * @response 200 baseResponse
   */
  async getRouters() {
    const { ctx } = this;
    return await ctx.service.resources.getRouters();
  }

}

module.exports = ResourcesController;
