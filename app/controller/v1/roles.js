'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 角色管理模块
 */

class RolesController extends Controller {

  /**
   * @apikey
   * @summary 获取角色列表
   * @description 获取角色列表接口
   * @request query string role_name 角色名称
   * @request query number pageSize 页数
   * @request query number current 页码
   * @router get /api/v1/roles
   */
  async index() {
    const { ctx } = this;
    const { allRule, query } = ctx.helper.findAllParamsDeal(
      ctx, {
        rule: ctx.rule.rolesPutReq,
        queryOrigin: ctx.query,
      });
    ctx.validate(allRule, query);
    return await ctx.service.roles.index(query);
  }

  /**
   * @apikey
   * @summary 获取角色详情
   * @description 获取角色详情接口
   * @request query number *id 角色id
   * @router get /api/v1/roles/getInfo
   */
  async getInfo() {
    const { ctx } = this;
    ctx.validate(ctx.rule.rolesChangeDefault, ctx.request.query);
    return await ctx.service.roles.getInfo(ctx.request.query);
  }

  /**
   * @summary 新增角色
   * @apikey
   * @description 新增角色接口
   * @router post /api/v1/roles
   * @request body rolesCreateReq
   * @response 200 baseResponse
   */
  async add() {
    const { ctx } = this;
    ctx.validate(ctx.rule.rolesCreateReq, ctx.request.body);
    return await ctx.service.roles.add(ctx.request.body);
  }


  /**
   * @summary 修改角色
   * @apikey
   * @description 修改角色接口
   * @router put /api/v1/roles
   * @request body rolesPutReq
   * @response 200 baseResponse
   */
  async update() {
    const { ctx } = this;
    ctx.validate(ctx.rule.rolesPutReq, ctx.request.body);
    return await ctx.service.roles.update(ctx.request.body);
  }

  /**
   * @summary 修改角色为默认角色
   * @apikey
   * @description 修改角色为默认角色接口
   * @router put /api/v1/roles/is_default
   * @request body rolesId
   * @response 200 baseResponse
   */
  async updateIsDefault() {
    const { ctx } = this;
    ctx.validate(ctx.rule.rolesChangeDefault, ctx.request.body);
    return await ctx.service.roles.updateIsDefault(ctx.request.body);
  }

  /**
   * @summary 删除角色
   * @apikey
   * @description 删除角色接口
   * @router delete /api/v1/roles
   * @request body rolesDelReq
   * @response 200 baseResponse
   */
  async destroy() {
    const { ctx } = this;
    ctx.validate(ctx.rule.rolesDelReq, ctx.request.body);
    return await ctx.service.roles.destroy(ctx.request.body);
  }
}

module.exports = RolesController;
