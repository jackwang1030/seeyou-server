'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 公共模块
 */
class CommonController extends Controller {

  /**
      * @summary 刷新token
      * @description 刷新token接口
      * @apikey
      * @router put /api/v1/refreshToken
      * @response 200 baseResponse
      */
  async refreshToken() {
    return await this.ctx.service.common.refreshToken();
  }


  /**
    * @summary 获取验证码
    * @description 获取验证码接口
    * @router get /api/v1/getCaptchaCode
    * @response 200 baseResponse
    */
  async getCaptchaCode() {
    return await this.ctx.service.common.getCaptchaCode();
  }

  /**
    * @summary 邮箱发送验证码
    * @description 邮箱发送验证码接口
    * @router get /api/v1/sendMailer
    * @request query string *email 邮箱
    * @response 200 baseResponse
    */
  async sendMailer() {
    const { ctx } = this;
    ctx.validate(ctx.rule.emailPostReq, ctx.request.query);
    return await this.ctx.service.common.sendMailer(ctx.request.query);
  }

  /**
    * @summary 上传文件
    * @description 上传文件接口
    * @router post /api/v1/file
    * @request query string *category 分类
    * @request query number *single 是否单文件 1 是 0 否
    * @response 200 baseResponse
    */
  async upload() {
    const { ctx } = this;
    ctx.validate(ctx.rule.fileUpload, ctx.request.query);
    return await ctx.service.file.uploadFile(ctx.request.query);
  }

  /**
    * @summary 邮箱验证
    * @description 邮箱验证接口
    * @router post /api/v1/emailVerify
    * @request body emailVerify
    * @response 200 baseResponse
    */
  async emailVerify() {
    const { ctx } = this;
    ctx.validate(ctx.rule.emailVerify, ctx.request.body);
    return await this.ctx.service.common.emailVerify(ctx.request.body);
  }


}

module.exports = CommonController;
