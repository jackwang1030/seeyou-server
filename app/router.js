'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  const tokenCheck = app.middleware.tokenVerify(app);

  // 重定向到swagger-ui.html
  app.router.redirect('/', '/swagger-ui.html', 302);
  // common
  router.put('/api/v1/refreshToken', controller.v1.common.refreshToken);
  router.get('/api/v1/getCaptchaCode', controller.v1.common.getCaptchaCode);
  router.get('/api/v1/sendMailer', controller.v1.common.sendMailer);
  router.post('/api/v1/file', controller.v1.common.upload);
  router.post('/api/v1/emailVerify', controller.v1.common.emailVerify);


  /** 系统用户模块 */
  router.post('/api/v1/admin_users', tokenCheck, controller.v1.adminUsers.create);
  router.get('/api/v1/admin_users', tokenCheck, controller.v1.adminUsers.index);
  router.put('/api/v1/admin_users', tokenCheck, app.middleware.auth('system:user:update'), controller.v1.adminUsers.update);
  router.put('/api/v1/admin_users/updateState', tokenCheck, controller.v1.adminUsers.updateState);
  router.post('/api/v1/admin_users/login', controller.v1.adminUsers.login);
  router.put('/api/v1/admin_users/updatePwd', tokenCheck, controller.v1.adminUsers.updatePwd);
  router.get('/api/v1/admin_users/getInfo', tokenCheck, controller.v1.adminUsers.getInfo);
  router.post('/api/v1/admin_users/logout', controller.v1.adminUsers.logout);


  /** 角色管理模块 */
  router.get('/api/v1/roles', tokenCheck, controller.v1.roles.index);
  router.get('/api/v1/roles/getInfo', tokenCheck, controller.v1.roles.getInfo);
  router.post('/api/v1/roles', tokenCheck, controller.v1.roles.add);
  router.put('/api/v1/roles', tokenCheck, controller.v1.roles.update);
  router.delete('/api/v1/roles', tokenCheck, controller.v1.roles.destroy);
  router.put('/api/v1/roles/is_default', tokenCheck, controller.v1.roles.updateIsDefault);

  /** 资源管理模块 */
  router.post('/api/v1/resources', tokenCheck, controller.v1.resources.add);
  router.put('/api/v1/resources', tokenCheck, controller.v1.resources.update);
  router.delete('/api/v1/resources', tokenCheck, controller.v1.resources.destroy);
  router.get('/api/v1/getAllResources', tokenCheck, controller.v1.resources.getAllResources);
  router.get('/api/v1/getRouters', tokenCheck, controller.v1.resources.getRouters);
  router.get('/api/v1/getTreeResources', tokenCheck, controller.v1.resources.getTreeResources);

  /** 系统配置模块 */
  router.get('/api/v1/settings', controller.v1.settings.index);
  router.post('/api/v1/settings', tokenCheck, controller.v1.settings.update);

  /** App用户模块 */
  router.post('/api/v1/app_users/login', controller.v1.appUsers.login);
  router.post('/api/v1/app_users/register', controller.v1.appUsers.register);

};
