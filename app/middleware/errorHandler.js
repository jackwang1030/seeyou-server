'use strict';

module.exports = () => {
  return async function errorHandler(ctx, next) {
    try {
      await next();
    } catch (err) {
      // 所有的异常都在 app 上触发一个 error 事件，框架会记录一条错误日志
      ctx.app.emit('error', err, ctx);

      const status = err.status || 500;
      // 生产环境时 500 错误的详细错误内容不返回给客户端，因为可能包含敏感信息
      const error = status === 500 && ctx.app.config.env === 'prod'
        ? 'Internal Server Error'
        : err.message;

      // 从 error 对象上读出各个属性，设置到响应中
      ctx.logger.error('接口错误拦截', error);
      const msg = error;
      ctx.helper.body.VALIDATION_FAILED({ ctx, msg });
      if (status === 422) {
        const msg = err.errors && err.errors.length > 0 ?
          (err.errors[0].code === 'missing_field' ? (ctx.app.config.env === 'prod' ? '参数检验失败'
            : `${err.errors[0].field}字段不能为空`)
            : err.errors[0].message) : '未知错误';
        ctx.helper.body.VALIDATION_FAILED({ ctx, msg });
      }
      ctx.status = status;
    }
  };
};
