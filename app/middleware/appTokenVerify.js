'use strict';

module.exports = app => {
  return async function tokenVerify(ctx, next) {
    // 获取token
    const token = ctx.request.header.authorization;
    // 验证token是否为空
    if (token) {
      const info = ctx.helper.analysisToken(token);
      const { data } = info;
      // 验证客户端token是否合法
      if (data) {
        const redis_token = await ctx.service.cache.get(data);
        if (!redis_token) {
          ctx.body = {
            code: 403,
            msg: '您的登录状态已过期，请重新登录',
            data: null,
          };
        } else if (token === redis_token) { // 验证是否为最新的token
          const uid = ctx.helper.getUserIdByHeader(ctx);
          const rsp = await app.mysql.query(`select state from t_app_users where id = ${uid}`);
          if (rsp[0].state === 0) {
            ctx.body = {
              code: 201,
              msg: '用户账号被封禁，无法访问App',
              data: null,
            };
            await app.redis.expire(data, 1);
            return;
          }
          await app.redis.expire(data, 60 * 60);
          await next();

        } else if (redis_token && (token !== redis_token)) {
          // 如果不是最新token，则代表用户在另一个机器上进行操作，需要用户重新登录保存最新token
          ctx.body = {
            code: 405,
            msg: '您的账号已在其他终端保持登录，如果继续将清除其他终端的登录状态',
            data: null,
          };
        }
      } else {
        // 如果token不合法，则代表客户端token已经过期或者不合法（伪造token）
        ctx.body = {
          code: 403,
          msg: '您的登录状态已过期，请重新登录',
          data: null,
        };
      }
    } else {
      // 如果token为空，则代表客户没有登录
      ctx.body = {
        code: 403,
        msg: '您还没有登录，请登录后再进行操作',
        data: null,
      };
    }
  };
};
