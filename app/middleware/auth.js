'use strict';

module.exports = name => { // 此处name为 auth(xxx) 的xxx
  return async function auth(ctx, next) {
    const userId = ctx.helper.getUserIdByHeader(ctx);
    await checkAuth(userId, ctx, next);
  };
  async function checkAuth(userId, ctx, next) {
    if (!name) {
      await next();
      return;
    }
    // 查询操作权限是否存在
    // const resource = await ctx.model.TResources.findOne({ where: { perms: name, kind: 2 }, raw: true });
    // 使用操作权限和按钮权限使用同一个
    const resource = await ctx.model.TResources.findOne({ where: { perms: name }, raw: true });
    if (!resource) {
      await next();
      return;
    }
    // 查询用户绑定的角色
    const roles = await ctx.model.TRolesUsers.findAll({ attributes: [ 'role_id' ], where: { user_id: userId } });
    const roleIds = roles.map(item => item.role_id);
    // role = 1 超级管理员
    if (roleIds.includes(1)) {
      await next();
      return;
    }
    const Op = ctx.app.Sequelize.Op;
    // 查询用户是否有菜单的权限
    const hasAccess = await ctx.model.TRolesResources.findOne({ where: { role_id: { [Op.in]: roleIds }, resource_id: resource.id } });
    if (!hasAccess) {
      ctx.body = {
        code: 201,
        msg: '权限不足',
        data: null,
      };
      return;
    }
    await next();
  }
};
