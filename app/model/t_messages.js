/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_messages', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    receiver_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    friend_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    msg_content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    type: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
    },
    is_read: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_messages',
  });

  Model.associate = function() {

  };

  return Model;
};
