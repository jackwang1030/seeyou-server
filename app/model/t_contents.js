/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_contents', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    type: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    video_id: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    img_ids: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    topic_ids: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    open_type: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    is_comment: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    is_draft: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    is_top: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0',
    },
    likes: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0',
    },
    sort: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 't_contents',
  });

  Model.associate = function() {

  };

  return Model;
};
