/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-11-29 20:33:35
 * @LastEditTime: 2022-01-06 13:12:38
 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_resources', {
    id: {
      type: DataTypes.STRING(60),
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING(60),
      allowNull: false,
    },
    name_cn: {
      type: DataTypes.STRING(60),
      allowNull: false,
    },
    path: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '',
    },
    parent_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
    },
    kind: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
    },
    icon: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    hidden: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '0',
    },
    status: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '1',
    },
    component: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '',
    },
    location: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    perms: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    access: {
      type: DataTypes.STRING(60),
      allowNull: true,
    },
    sort: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      defaultValue: '0',
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_resources',
  });

  Model.associate = function() {
    // 与roles存在多对多关系，使用belongsToMany()
    app.model.TResources.belongsToMany(app.model.TRoles, {
      through: app.model.TRolesResources,
      foreignKey: 'resource_id',
      otherKey: 'role_id',
    });
  };

  return Model;
};
