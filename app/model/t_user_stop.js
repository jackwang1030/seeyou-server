/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_user_stop', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    is_stop: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
    },
    stop_start_time: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    stop_end_time: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_user_stop',
  });

  Model.associate = function() {

  };

  return Model;
};
