/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_chat_group', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    group_uid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    group_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    group_cover: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_chat_group',
  });

  Model.associate = function() {

  };

  return Model;
};
