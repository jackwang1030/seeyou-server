/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_topic', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    category: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
    },
    topic_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    is_top: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0',
    },
    sort: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 't_topic',
  });

  Model.associate = function() {

  };

  return Model;
};
