'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_settings', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      required: true,
      description: 'id',
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    title_en: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    slogan: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    logo: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    version: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    iconfontProjectUrl: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    iconfontUrl: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 't_settings',
  });

  return Model;
};
