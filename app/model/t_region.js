/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_region', {
    region_code: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    region_name: {
      type: DataTypes.STRING(40),
      allowNull: true,
    },
    parent_code: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    simple_name: {
      type: DataTypes.STRING(40),
      allowNull: true,
    },
    level: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    city_code: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    zip_code: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    mer_name: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    lng: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    pinyin: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
  }, {
    tableName: 't_region',
  });

  Model.associate = function() {

  };

  return Model;
};
