/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_check', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    type: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    obj_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    checker_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    checker_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_check',
  });

  Model.associate = function() {

  };

  return Model;
};
