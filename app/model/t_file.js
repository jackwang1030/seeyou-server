'use strict';

const dayjs = require('dayjs');
module.exports = app => {
  const { STRING, INTEGER, DATE } = app.Sequelize;
  return app.model.define('t_file', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    path: STRING,
    name: STRING,
    category: STRING,
    ext: STRING,
    size: STRING,
    md5: STRING,
    create_time: {
      type: DATE,
      get() {
        const time = this.getDataValue('create_time');
        return time ? dayjs(time).format('YYYY-MM-DD HH:mm:ss') : null;
      },
    },
    update_time: {
      type: DATE,
      get() { // 获取器，获取数据时候格式化时间
        const time = this.getDataValue('update_time');
        return time ? dayjs(time).format('YYYY-MM-DD HH:mm:ss') : null;
      },
    },
    delete_time: DATE,
  }, {
    freezeTableName: false,
    tableName: 't_file',
    underscored: false,
    paranoid: true,
    timestamps: true, // 开启自动时间才能使用逻辑删除
    createdAt: false,
    updatedAt: false,
    deletedAt: 'delete_time', // 逻辑删除字段
  });
};

