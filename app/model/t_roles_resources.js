/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-11-29 20:33:35
 * @LastEditTime: 2021-12-21 10:31:03
 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_roles_resources', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    role_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    resource_id: {
      type: DataTypes.STRING(60),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_roles_resources',
  });

  Model.associate = function() {

  };

  return Model;
};
