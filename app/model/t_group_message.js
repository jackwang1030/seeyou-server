/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_group_message', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    group_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    send_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    send_msg: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    msg_type: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_group_message',
  });

  Model.associate = function() {

  };

  return Model;
};
