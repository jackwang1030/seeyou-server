'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_dict_info', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    type_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    parent_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    label: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    order: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
  }, {
    tableName: 't_dict_info',
  });

  Model.associate = function() {
    // 与DictType存在多对一关系，所以使用belongsTo()
    app.model.TDictInfo.belongsTo(app.model.TDictType, { foreignKey: 'type_id', targetKey: 'id' });
  };

  return Model;
};
