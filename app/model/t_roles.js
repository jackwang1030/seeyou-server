'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_roles', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    role_name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      primaryKey: true,
    },
    is_default: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 't_roles',
  });

  Model.associate = function() {
    // 与admin-app存在多对多关系，使用belongsToMany()
    // app.model.TRoles.belongsToMany(app.model.TAdminUsers, {
    //   through: app.model.TRolesUsers,
    //   foreignKey: 'role_id',
    //   otherKey: 'user_id',
    // });
    // 与resources存在多对多关系，使用belongsToMany()
    app.model.TRoles.belongsToMany(app.model.TResources, {
      through: app.model.TRolesResources,
      foreignKey: 'role_id',
      otherKey: 'resource_id',
    });
  };

  return Model;
};
