/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-11-29 20:33:35
 * @LastEditTime: 2021-11-30 22:23:21
 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_roles_users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    role_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }
  //  {
  //   tableName: 't_roles_users',
  // }
  );

  Model.associate = function() {
    app.model.TRolesUsers.belongsTo(app.model.TRoles, {
      foreignKey: 'role_id',
      targetKey: 'id',
    });
  };

  return Model;
};
