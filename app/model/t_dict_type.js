'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_dict_type', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    key: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    order: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
  }, {
    tableName: 't_dict_type',
  });

  Model.associate = function() {
    // dictType与dictInfo是一对多关系
    app.model.TDictType.hasMany(app.model.TDictInfo, { foreignKey: 'type_id', targetKey: 'id' });
  };

  return Model;
};
