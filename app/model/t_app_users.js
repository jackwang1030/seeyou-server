/* indent size: 2 */
'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('t_app_users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    user_no: {
      type: DataTypes.STRING(12),
      allowNull: false,
      primaryKey: true,
    },
    phone: {
      type: DataTypes.STRING(11),
      allowNull: true,
    },
    nick_name: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    real_name: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    signature: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    intro: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    age: {
      type: DataTypes.INTEGER(3),
      allowNull: true,
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    constellation: {
      type: DataTypes.STRING(6),
      allowNull: true,
    },
    gender: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      defaultValue: '3',
    },
    school: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    birthday: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    province: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    city: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    qq: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1',
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    last_login: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    paranoid: true, // 如果我们开启了paranoid（偏执）模式，destroy的时候不会执行DELETE语句，而是执行一个UPDATE语句将deleted_at字段设置为当前时间
    defaultScope: {
      attributes: { exclude: [ 'password' ] },
    },
    scopes: {
      withPassword: {
        attributes: {},
      },
    },
  },
  {
    tableName: 't_app_users',
  });

  Model.associate = function() {

  };

  return Model;
};
