'use strict';

module.exports = app => {
  const { validator } = app;
  // 自定义校验规则
  validator.addRule('username', (rule, value) => {
    const reg = RegExp(/^[a-zA-Z0-9_]{0,}$/);
    if (!reg.test(value)) {
      return '账号不能含有中文或特殊字符';
    }
    if (value.length < 6 || value.length > 12) {
      return '账号长度应该在6-12之间';
    }
  });
  validator.addRule('email', (rule, value) => {
    const reg = RegExp(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/);
    if (!reg.test(value)) {
      return '邮箱格式不正确';
    }
  });
  validator.addRule('phone', (rule, value) => {
    const reg = /^1[3|4|5|6|7|8][0-9]\d{8}$/;
    if (!reg.test(value)) {
      return '手机号格式不正确';
    }
  });
  validator.addRule('password', (rule, value) => {
    // const reg = new RegExp('(?=.*[0-9])(?=.*[a-zA-Z]).{6,18}');
    // if (!reg.test(value)) {
    //   return '密码中必须包含字母，数字，至少6个字符，最多18个字符';
    // }
    if (!value.trim()) {
      return '密码不能为空';
    }
  });
};

