'use strict';

const Service = require('egg').Service;

class SettingsService extends Service {
  async index() {
    const { ctx } = this;
    const rsp = await ctx.model.TSettings.findAll({ raw: true });
    return ctx.helper.body.SUCCESS({ ctx, data: rsp[0], msg: '系统配置获取成功' });
  }

  async update(payload) {
    const { ctx } = this;
    await ctx.model.TSettings.update(payload, { where: { id: payload.id } });
    return ctx.helper.body.SUCCESS({ ctx, msg: '系统配置信息修改成功', data: null });
  }
}

module.exports = SettingsService;
