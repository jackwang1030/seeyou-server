'use strict';

const Service = require('egg').Service;
const { Op } = require('sequelize');

class AdminUserService extends Service {
  /**
   * @description: 获取系统用户列表
   * @param {*} payload
   * @param {*} query
   * @return {*}
   */
  async index(payload, query) {
    const { ctx } = this;
    const { pageSize, current } = payload;
    const { startTime, endTime, orderColumn, orderType } = query;
    const where = payload.where;
    const Order = [];
    orderColumn && orderType ? Order.push([ orderColumn, orderType ]) : null;
    // 如果请求者不是用户id为1的超级管理员，则不返回id为1的超级管理员用户
    if (ctx.helper.getUserIdByHeader(ctx) !== 1) {
      if (where[Op.and]) {
        where[Op.and].push({ id: { [Op.ne]: 1 } });
      } else {
        where[Op.and] = [{ id: { [Op.ne]: 1 } }];
      }
    }
    if (startTime && endTime) {
      where[Op.and] = [{ created_at: { [Op.between]: [ startTime, endTime ] } }];
    }
    const rsp = await ctx.model.TAdminUsers.findAndCountAll({
      limit: pageSize,
      offset: current ? current - 1 : 0,
      where,
      attributes: { exclude: [ 'deleted_at' ] },
      order: Order,
      include: [
        {
          attributes: [ 'id', 'role_name', 'is_default' ],
          model: ctx.model.TRoles,
        },
      ],
      distinct: true,
    });
    // ctx.logger.info('===获取用户列表===', rsp);
    if (rsp) {
      return ctx.helper.body.SUCCESS({
        ctx,
        msg: '用户列表获取成功',
        data: { total: rsp.count, list: rsp.rows, current, pageSize },
      });
    }
    return ctx.helper.body.ERROR({ ctx, msg: '用户列表获取失败' });
  }

  /**
   * @description: 创建系统用户
   * @param {*} data
   * @return {*}
   */
  async create(data) {
    const { ctx, app } = this;
    const { phone, email, password } = data;
    const transaction = await ctx.model.transaction();
    try {
      const phoneUser = await this.existsUserUniqueFields({ phone });
      const emailUser = await this.existsUserUniqueFields({ email });
      if (phoneUser) {
        return ctx.helper.body.ERROR({ ctx, msg: '手机号已存在' });
      }
      if (emailUser) {
        return ctx.helper.body.ERROR({ ctx, msg: '邮箱已存在' });
      }
      const user_no = ctx.helper.randomNum(8);
      const rsaDecryptPwd = ctx.helper.rsaDecrypt(password);
      const aesEncryptPwd = ctx.helper.aesEncrypt(rsaDecryptPwd, app.config.jwt.secret);
      const rsp = await ctx.model.TAdminUsers.create({ ...data, password: aesEncryptPwd, user_no }, { transaction });
      ctx.logger.info('=========rsp=====', rsp.dataValues);
      if (rsp) {
        const role = await ctx.model.TRoles.findOne({ where: { is_default: 1 }, raw: true });
        ctx.logger.info('=========role=====', role);
        if (role) {
          await ctx.model.TRolesUsers.create({ user_id: rsp.dataValues.id, role_id: role.id }, { transaction });
          await transaction.commit();
          return ctx.helper.body.SUCCESS({ ctx, msg: '用户新增成功', data: null });
        }
      }
      return ctx.helper.body.ERROR({ ctx, msg: '用户新增失败' });
    } catch (err) {
      ctx.logger.error(err);
      await transaction.rollback();
      return ctx.helper.body.ERROR({ ctx, msg: '用户新增失败' });
    }

  }

  /**
   * @description: 修改用户信息
   * @param {*} data
   * @return {*}
   */
  async update(data) {
    const { ctx } = this;
    const { phone, email, roles, id } = data;
    // const uid = ctx.helper.getUserIdByHeader(ctx);
    const transaction = await ctx.model.transaction();
    try {
      const phoneUser = await this.existsUserUniqueFields({ phone });
      const emailUser = await this.existsUserUniqueFields({ email });
      if (phoneUser && id !== phoneUser.id) {
        return ctx.helper.body.ERROR({ ctx, msg: '手机号已存在' });
      }
      if (emailUser && id !== emailUser.id) {
        return ctx.helper.body.ERROR({ ctx, msg: '邮箱已存在' });
      }
      await ctx.model.TRolesUsers.destroy({ where: { user_id: id } }, { transaction });
      const arr = [];
      roles.forEach(item => {
        arr.push({ role_id: item, user_id: id });
      });
      await ctx.model.TRolesUsers.bulkCreate(arr, { transaction });
      await ctx.model.TAdminUsers.update({ ...data }, { where: { id } }, { transaction });
      await transaction.commit();
      return ctx.helper.body.SUCCESS({ ctx, msg: '用户信息修改成功', data: null });
    } catch (err) {
      ctx.logger.error(err);
      await transaction.rollback();
      return ctx.helper.body.ERROR({ ctx, msg: '用户信息修改失败' });
    }

  }

  /**
   * @description: 登录接口
   * @param {*} payload
   * @return {*}
   */
  async login(payload) {
    const { ctx, app } = this;
    const { user_no, password, captchaCode, uuid, type, email } = payload;
    if (![ 1, 2 ].includes(type)) {
      return ctx.helper.body.ERROR({ ctx, msg: '登录方式不正确' });
    }
    // 账号密码登录
    if (type === 1) {
      if (!user_no || !password) {
        return ctx.helper.body.ERROR({ ctx, msg: '账号密码不能为空' });
      }
      // 如果不传uuid字段，不验证验证码
      if (uuid || uuid === null) {
        const { code, msg } = await ctx.helper.verifyCaptchaCode(
          ctx,
          captchaCode,
          uuid
        );
        if (code !== 0) {
          return ctx.helper.body.ERROR({ ctx, msg });
        }
      }
      const user = await ctx.model.TAdminUsers.scope('withPassword').findOne({
        where: { user_no },
      });
      if (!user) {
        return ctx.helper.body.NOT_FOUND({ ctx, msg: '用户不存在' });
      }
      const rsaDecryptPwd = ctx.helper.rsaDecrypt(password);
      ctx.logger.info('======解密密码======', rsaDecryptPwd);
      const aesDecryptPwd = ctx.helper.aesEncrypt(rsaDecryptPwd, app.config.jwt.secret);
      ctx.logger.info('======加密密码======', aesDecryptPwd);
      if (user.password !== aesDecryptPwd) {
        return ctx.helper.body.ERROR({ ctx, msg: '密码不正确' });
      }
      const result = await ctx.model.TAdminUsers.findOne({
        where: {
          user_no,
          password: aesDecryptPwd,
        },
        raw: true,
      });
      ctx.logger.info('===========user===========', result);
      return await this.loginDeal(ctx, result);
    }
    // 邮箱验证码登录
    if (type === 2) {
      if (!captchaCode) {
        return ctx.helper.body.ERROR({ ctx, msg: '请输入验证码' });
      }
      const rsp = await ctx.model.TAdminUsers.findOne({ where: { email }, raw: true });
      if (rsp) {
        const cacheCode = await ctx.service.cache.get(`code_${rsp.email}`);
        if (!cacheCode) {
          return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确或已过期' });
        } else if (cacheCode === captchaCode) {
          await ctx.service.cache.set(`code_${email}`, captchaCode, 1);
          return await this.loginDeal(ctx, rsp);
        }
        return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确' });
      }
      return ctx.helper.body.ERROR({ ctx, msg: '邮箱未绑定用户' });
    }
  }

  /**
   * @description: 登录用户，获取到user后处理
   * @param {*} ctx
   * @param {*} user
   * @return {*}
   */
  async loginDeal(ctx, user) {
    if (user.state !== 1) {
      return ctx.helper.body.ERROR({ ctx, msg: '用户被禁止登录' });
    }
    const token = ctx.helper.generateToken(user.id, 60 * 1000 * 24);
    await ctx.service.cache.set(user.id, token, 60 * 60);
    return ctx.helper.body.LOGIN_SUCCESS({
      data: user,
      ctx,
      msg: '登录成功',
      token,
    });
  }


  /**
   * @description: 退出登录
   * @return {*}
   */
  async logout() {
    const { ctx } = this;
    const token = ctx.request.headers.authorization;
    const uid = ctx.helper.getUserIdByHeader(ctx);
    await ctx.service.cache.set(uid, token, 1);
    return ctx.helper.body.SUCCESS({ ctx, msg: '退出成功' });
  }

  /**
   * @description: 获取个人信息
   * @return {*}
   */
  async getInfo() {
    const { ctx } = this;
    const uid = ctx.helper.getUserIdByHeader(ctx);
    const settings = await ctx.model.TSettings.findAll({ raw: true });
    const data = await ctx.model.TAdminUsers.findOne({
      include: [
        {
          model: ctx.model.TRoles,
        },
      ],
      where: {
        id: uid,
      },
    });
    return ctx.helper.body.SUCCESS({ ctx, msg: '用户信息获取成功', data: { userInfo: data, settings: settings[0] } });
  }

  /**
   * @description: 系统用户修改密码
   * @param {*} payload
   * @return {*}
   */
  async updatePwd(payload) {
    const { ctx, app } = this;
    const { password, captchaCode } = payload;
    const rsaDecryptPwd = ctx.helper.rsaDecrypt(password);
    const aesEncryptPwd = ctx.helper.aesEncrypt(rsaDecryptPwd, app.config.jwt.secret);
    const uid = ctx.helper.getUserIdByHeader(ctx);
    const user = await ctx.model.TAdminUsers.scope('withPassword').findOne({ where: { id: uid }, raw: true });
    if (user) {
      const cacheCode = await ctx.service.cache.get(`code_${user.email}`);
      if (!cacheCode) {
        return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确或已过期' });
      } else if (cacheCode === captchaCode) {
        await ctx.model.TAdminUsers.update({ password: aesEncryptPwd }, { where: { id: uid } });
        return ctx.helper.body.SUCCESS({ ctx, msg: '密码修改成功', data: null });
      }
      return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确' });
    }
  }


  /**
   * @description: 修改系统用户状态
   * @param {*} payload
   * @return {*}
   */
  async updateState(payload) {
    const { ctx } = this;
    const { id, state } = payload;
    if (![ 0, 1 ].includes(state)) {
      return ctx.helper.body.ERROR({ ctx, msg: 'state值必须为0或者1' });
    }
    await ctx.model.TAdminUsers.update({ state }, { where: { id } });
    return ctx.helper.body.SUCCESS({ ctx, msg: '修改成功', data: null });
  }

  /**
   * @description: 某个字段数据是否存在
   * @param {*} payload
   * @return {*}
   */
  async existsUserUniqueFields(payload) {
    const { ctx } = this;
    const { user_no, email, phone } = payload;
    const where = {};
    where[Op.or] = [];
    user_no ? where[Op.or].push({ user_no }) : null;
    email ? where[Op.or].push({ email }) : null;
    phone ? where[Op.or].push({ phone }) : null;
    return await ctx.model.TAdminUsers.findOne({
      where,
      attributes: { exclude: [ 'password', 'deleted_at' ] },
      raw: true,
    });
  }
}

module.exports = AdminUserService;
