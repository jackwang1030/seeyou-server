'use strict';

const Service = require('egg').Service;
// const { Op } = require('sequelize');

class AppUserService extends Service {

  /**
   * @description: 登录接口
   * @param {*} payload
   * @return {*}
   */
  async login(payload) {
    const { ctx, app } = this;
    const { user_no, password, captchaCode, uuid, type, email } = payload;
    if (![ 1, 2 ].includes(type)) {
      return ctx.helper.body.ERROR({ ctx, msg: '登录方式不正确' });
    }
    // 账号密码登录
    if (type === 1) {
      if (!user_no || !password) {
        return ctx.helper.body.ERROR({ ctx, msg: '账号密码不能为空' });
      }
      // 如果不传uuid字段，不验证验证码
      if (uuid || uuid === null) {
        const { code, msg } = await ctx.helper.verifyCaptchaCode(
          ctx,
          captchaCode,
          uuid
        );
        if (code !== 0) {
          return ctx.helper.body.ERROR({ ctx, msg });
        }
      }
      const user = await ctx.model.TAppUsers.scope('withPassword').findOne({
        where: { user_no },
      });
      if (!user) {
        return ctx.helper.body.NOT_FOUND({ ctx, msg: '用户不存在' });
      }
      const rsaDecryptPwd = ctx.helper.rsaDecrypt(password);
      ctx.logger.info('======解密密码======', rsaDecryptPwd);
      const aesDecryptPwd = ctx.helper.aesEncrypt(rsaDecryptPwd, app.config.jwt.secret);
      ctx.logger.info('======加密密码======', aesDecryptPwd);
      if (user.password !== aesDecryptPwd) {
        return ctx.helper.body.ERROR({ ctx, msg: '密码不正确' });
      }
      const result = await ctx.model.TAppUsers.findOne({
        where: {
          user_no,
          password: aesDecryptPwd,
        },
        raw: true,
      });
      ctx.logger.info('===========user===========', result);
      return await this.loginDeal(ctx, result);
    }
    // 邮箱验证码登录
    if (type === 2) {
      if (!captchaCode) {
        return ctx.helper.body.ERROR({ ctx, msg: '请输入验证码' });
      }
      const rsp = await ctx.model.TAppUsers.findOne({ where: { email }, raw: true });
      if (rsp) {
        const cacheCode = await ctx.service.cache.get(`code_${rsp.email}`);
        if (!cacheCode) {
          return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确或已过期' });
        } else if (cacheCode === captchaCode) {
          await ctx.service.cache.set(`code_${email}`, captchaCode, 1);
          return await this.loginDeal(ctx, rsp);
        }
        return ctx.helper.body.ERROR({ ctx, msg: '验证码不正确' });
      }
      return ctx.helper.body.ERROR({ ctx, msg: '邮箱未绑定用户' });
    }
  }

  /**
   * @description: 登录用户，获取到user后处理
   * @param {*} ctx
   * @param {*} user
   * @return {*}
   */
  async loginDeal(ctx, user) {
    if (user.state !== 1) {
      return ctx.helper.body.ERROR({ ctx, msg: '用户被禁止登录' });
    }
    const token = ctx.helper.generateToken(`app_${user.id}`, 60 * 1000 * 24);
    await ctx.service.cache.set(`app_${user.id}`, token, 60 * 60);
    await ctx.model.TAppUsers.update({ last_login: new Date() }, { where: { id: user.id } });
    return ctx.helper.body.LOGIN_SUCCESS({
      data: user,
      ctx,
      msg: '登录成功',
      token,
    });
  }

  /**
   * @description: app用户注册
   * @param {*} payload
   * @return {*}
   */
  async register(payload) {
    const { ctx } = this;
    const user = await ctx.model.TAppUsers.findOne({ attributes: [ 'id' ], where: { email: payload.email } });
    if (user) {
      return ctx.helper.body.ERROR({ ctx, msg: '邮箱已被注册使用' });
    }
    const user_no = ctx.helper.randomNum(8);
    // const rsaDecryptPwd = ctx.helper.rsaDecrypt(payload.password);
    // const aesEncryptPwd = ctx.helper.aesEncrypt(rsaDecryptPwd, app.config.jwt.secret);
    await ctx.model.TAppUsers.create({ ...payload, last_login: new Date(), user_no }, { raw: true });
    return ctx.helper.body.SUCCESS({ ctx, msg: '用户注册成功' });
  }
}

module.exports = AppUserService;
