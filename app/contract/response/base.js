'use strict';

module.exports = {
  baseResponse: {
    code: { type: 'integer', required: true, example: 0 },
    data: { type: 'string', example: '{}' },
    msg: { type: 'string', example: '请求成功' },
  },
};
