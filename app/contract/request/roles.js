'use strict';

const body = {
  rolesId: {
    id: { type: 'number', required: true, description: 'id' },
  },
  rolesAdd: {
    role_name: {
      type: 'string',
      max: 20,
      required: true,
      trim: true,
      like: true, // 是否模糊查询
      description: '角色名称',
    },
    remark: {
      type: 'string',
      required: false,
      trim: true,
      description: '备注',
    },
    resourceIds: {
      type: 'string',
      required: true,
      description: "资源id, 多个用','隔开",
    },
  },
};

module.exports = {
  ...body,
  rolesCreateReq: {
    ...body.rolesAdd,
  },
  rolesChangeDefault: {
    ...body.rolesId,
  },
  rolesPutReq: {
    ...body.rolesId,
    ...body.rolesAdd,
  },
  rolesDelReq: {
    ids: {
      type: 'array',
      required: true,
      itemType: 'number',
      description: 'ids',
      example: [ 1, 2 ],
    },
  },
};
