/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-16 22:01:40
 * @LastEditTime: 2022-01-06 14:25:22
 */
'use strict';

const body = {
  resourcesId: {
    id: { type: 'string', required: true, description: 'id' },
  },
  resourcesAddReq: {
    name_cn: {
      type: 'string',
      required: true,
      format: /^(?:[\u3400-\u4DB5\u4E00-\u9FEA\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\uD840-\uD868\uD86A-\uD86C\uD86F-\uD872\uD874-\uD879][\uDC00-\uDFFF]|\uD869[\uDC00-\uDED6\uDF00-\uDFFF]|\uD86D[\uDC00-\uDF34\uDF40-\uDFFF]|\uD86E[\uDC00-\uDC1D\uDC20-\uDFFF]|\uD873[\uDC00-\uDEA1\uDEB0-\uDFFF]|\uD87A[\uDC00-\uDFE0])+$/,
      like: true,
      description: '资源名称',
    },
    name: {
      type: 'string',
      required: true,
      format: /^[A-Za-z0-9]+$/,
      like: true,
      description: '资源中文名称',
    },
    path: {
      type: 'string',
      required: false,
      max: 100,
      description: '资源路径',
    },
    parent_id: {
      type: 'string',
      required: true,
      max: 50,
      description: '父菜单id',
    },
    kind: {
      type: 'number',
      required: true,
      description: '资源类型 1-菜单, 2-操作, 3-外部菜单, 4-目录, 5-按钮',
    },
    icon: {
      type: 'string',
      required: false,
      max: 255,
      description: '图标',
    },
    hidden: {
      type: 'number',
      required: false,
      description: '是否隐藏此路由 1-是, 0-否',
      example: '0',
    },
    status: {
      type: 'number',
      required: false,
      description: '菜单状态 1-启用, 2-停用',
      example: '1',
    },
    component: {
      type: 'string',
      required: false,
      max: 255,
      description: '路由对应的组件',
    },
    perms: {
      type: 'string',
      required: false,
      max: 100,
      description: '权限标识',
    },
    sort: {
      type: 'number',
      required: true,
      description: '排序',
    },
    remark: {
      type: 'string',
      required: false,
      trim: true,
      description: '备注',
    },
  },
  resourcesSearchReq: {
    status: {
      type: 'number',
      required: false,
      description: '菜单状态 1-启用, 2-停用',
      example: '1',
    },
    name_cn: {
      type: 'string',
      required: false,
      like: true,
      description: '资源中文名称',
    },
    type: {
      type: 'number',
      required: false,
      description: '1: list形式, 2: tree形式',
      example: '2',
    },
  },
};


module.exports = {
  ...body,
  resourcesReq: {
    // ...body.resourcesId,
    ...body.resourcesAddReq,
  },
  resourcesPutReq: {
    ...body.resourcesId,
    ...body.resourcesAddReq,
  },
  resourcesSeaReq: {
    ...body.resourcesSearchReq,
  },
  resourcesDelReq: {
    ...body.resourcesId,
  },
};
