'use strict';

const body = {
  appUserId: {
    id: { type: 'number', required: true, description: 'id' },
  },
  appUserReq: {
    phone: {
      type: 'string',
      required: true,
      trim: true,
      format: /^(?:(?:\+|00)86)?1[3-9]\d{9}$/,
      example: '15878451250',
      description: '手机号',
    },
    avatar: {
      type: 'string',
      required: false,
      trim: true,
      description: '头像',
    },
    username: {
      type: 'string',
      required: true,
      min: 2,
      max: 60,
      trim: true,
      like: true,
      example: '柠檬味的糖果',
      description: '用户名',
    },
    password: {
      type: 'string',
      required: false,
      trim: true,
      example: 'U2FsdGVkX1+d2trn168lvrltowbn9wBF8vx2rcEeEeQ=',
      description: '密码',
    },
    email: {
      type: 'string',
      required: true,
      max: 60,
      like: true,
      trim: true,
      format: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
      example: '123@qq.com',
      description: '邮箱',
    },
    gender: {
      trim: true,
      required: false,
      type: 'number',
      example: '性别：1.男，2.女，3.保密',
    },
    type: {
      type: 'number',
      trim: true,
      required: false,
      example: '类型 1、超级管理员 2、普通管理员',
    },
    state: {
      type: 'number',
      trim: true,
      required: false,
      example: '是否停用 1、启用，0、停用',
    },
  },
  appUserPutReq: {
    username: {
      type: 'string',
      required: false,
      min: 2,
      max: 60,
      trim: true,
      like: true,
      example: '柠檬味的糖果',
      description: '用户名',
    },
    phone: {
      type: 'string',
      required: true,
      trim: true,
      format: /^(?:(?:\+|00)86)?1[3-9]\d{9}$/,
      example: '15878451250',
      description: '手机号',
    },
    email: {
      type: 'string',
      required: true,
      max: 60,
      like: true,
      trim: true,
      format: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
      example: '123@qq.com',
      description: '邮箱',
    },
    gender: {
      trim: true,
      required: false,
      type: 'number',
      example: '性别：1.男，2.女，3.保密',
    },
    state: {
      type: 'number',
      trim: true,
      required: false,
      example: '是否停用 1、启用，0、停用',
    },
    remark: {
      type: 'string',
      required: false,
      trim: true,
      description: '备注',
    },
  },
  appUserSearchReq: {
    user_no: {
      type: 'string',
      required: false,
      trim: true,
      like: true,
      description: '账号',
    },
    phone: {
      type: 'string',
      required: false,
      trim: true,
      description: '手机号',
    },
    username: {
      type: 'string',
      required: false,
      min: 2,
      max: 60,
      trim: true,
      like: true,
      description: '用户名',
    },
    email: {
      type: 'string',
      required: false,
      max: 60,
      like: true,
      trim: true,
      description: '邮箱',
    },
    gender: {
      trim: true,
      required: false,
      type: 'number',
    },
    state: {
      type: 'number',
      trim: true,
      required: false,
    },
  },
  appUserRegisterReq: {
    email: {
      type: 'string',
      required: true,
      max: 60,
      format: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
      description: '邮箱',
    },
    avatar: {
      type: 'string',
      required: false,
      description: '头像url',
    },
    nick_name: {
      type: 'string',
      max: 60,
      trim: true,
      like: true,
      description: '昵称',
    },
    signature: {
      type: 'string',
      max: 60,
      trim: true,
      like: true,
      description: '个性签名',
    },
    gender: {
      required: false,
      type: 'number',
      description: '性别：1.男，2.女',
    },
    password: {
      type: 'string',
      required: false,
      trim: true,
      description: '密码',
    },
  },
};

module.exports = {
  ...body,
  appUserPutReq: {
    ...body.appUserId,
    ...body.appUserPutReq,
  },
  appUserRegisterReq: {
    ...body.appUserRegisterReq,
  },
  appUserSearchReq: {
    ...body.appUserSearchReq,
  },
  appUserChangeState: {
    ...body.appUserId,
    state: {
      type: 'number',
      trim: true,
      required: true,
      description: '是否停用 1、启用，0、停用',
    },
  },
  appUserUpPwdReq: {
    password: {
      type: 'string',
      required: false,
      trim: true,
      example: 'U2FsdGVkX1+d2trn168lvrltowbn9wBF8vx2rcEeEeQ=',
      description: '密码',
    },
    captchaCode: {
      type: 'string',
      required: true,
      max: 60,
      trim: true,
      example: '000000',
      description: '验证码',
    },
  },
  appUserLoginReq: {
    user_no: {
      type: 'string',
      required: false,
      trim: true,
      like: true,
      example: 'root0001',
      description: '账号',
    },
    password: {
      type: 'string',
      required: false,
      trim: true,
      example: 'U2FsdGVkX1+d2trn168lvrltowbn9wBF8vx2rcEeEeQ=',
      description: '密码',
    },
    email: {
      type: 'string',
      required: false,
      max: 60,
      like: true,
      trim: true,
      format: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
      example: '2726653264@qq.com',
      description: '邮箱',
    },
    type: {
      trim: true,
      required: true,
      type: 'number',
      example: '1',
      description: '登录方式 1： 账号密码登录，2：邮箱登录',
    },
    captchaCode: {
      type: 'string',
      required: false,
      max: 60,
      trim: true,
      example: '000000',
      description: '验证码',
    },
    uuid: {
      type: 'string',
      required: false,
      trim: true,
      description: 'uuid',
    },
  },
  appUserDelReq: {
    ids: {
      type: 'array',
      required: true,
      itemType: 'number',
      description: 'ids',
      example: [ 1, 2 ],
    },
  },
};
