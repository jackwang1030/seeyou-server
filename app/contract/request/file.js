'use strict';

const body = {
  fileUpload: {
    category: {
      type: 'string',
      required: true,
      description: '文件分类',
    },
    single: {
      type: 'number',
      required: true,
      description: '是否单文件 1 是 0 否',
    },
  },
};

module.exports = {
  ...body,
  fileUpload: {
    ...body.fileUpload,
  },
};
