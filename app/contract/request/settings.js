'use strict';

const body = {
  settings: {
    id: { type: 'number', required: true, description: 'id' },
    title: {
      type: 'string',
      required: false,
      trim: true,
      description: '系统名称',
    },
    logo: {
      type: 'string',
      required: false,
      trim: true,
      description: '系统logo',
    },
    version: {
      type: 'string',
      required: false,
      trim: true,
      description: '版本号',
    },
    title_en: {
      type: 'string',
      required: false,
      trim: true,
      description: '系统英文名称',
    },
    iconfontProjectUrl: {
      type: 'string',
      required: false,
      trim: true,
      description: 'iconfont项目地址',
    },
    iconfontUrl: {
      type: 'string',
      required: false,
      trim: true,
      description: 'iconfont js在线地址',
    },
  },
};
module.exports = {
  ...body,
  settings: {
    ...body.settings,
  },
};
