'use strict';

const body = {
  codeReq: {
    code: {
      type: 'string',
      required: true,
      max: 60,
      like: true,
      trim: true,
      description: '验证码',
    },
    is_reg: {
      type: 'number',
      required: false,
      description: '是否是注册验证',
    },
  },
  emailReq: {
    email: {
      type: 'string',
      required: true,
      max: 60,
      like: true,
      trim: true,
      format: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
      example: '123@qq.com',
      description: '邮箱',
    },
  },
};

module.exports = {
  ...body,
  emailPostReq: {
    ...body.emailReq,
  },
  emailVerify: {
    ...body.codeReq,
    ...body.emailReq,
  },
};
