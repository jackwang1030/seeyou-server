/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-11-29 19:15:02
 * @LastEditTime: 2021-11-30 16:58:08
 */
'use strict';

const path = require('path');

module.exports = app => {
  // 加载所有的校验规则
  const directory = path.join(app.config.baseDir, 'app/validate');
  app.loader.loadToApp(directory, 'validate');

};

